package com.example.ejemplosqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.ejemplosqlite.entidades.Usuario;
import com.example.ejemplosqlite.utilidades.utilidades;

import java.util.ArrayList;

public class Consultar_listView extends AppCompatActivity {
    ListView listViewPersonas;
    ArrayList<String> listaInformacion;
    ArrayList<Usuario> listaUsuario;

    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consultar_list_view);

        conn = new ConexionSQLiteHelper(getApplicationContext(),"bd_usuarios",null,1);
        listViewPersonas = (ListView)findViewById(R.id.listViewPersonas);


        consultaListaPersonas();
        ArrayAdapter adaptador=new ArrayAdapter(this,android.R.layout.simple_list_item_1,listaInformacion);
        listViewPersonas.setAdapter(adaptador);

    }

    private void consultaListaPersonas() {
        SQLiteDatabase db=conn.getReadableDatabase();
        Usuario usuario = null;
        listaUsuario = new ArrayList<Usuario>();

        Cursor cursor = db.rawQuery("SELECT * FROM "+utilidades.TABLA_Tegnologia,null);

        while(cursor.moveToNext()){
            usuario = new Usuario();
            usuario.setImei(cursor.getInt(0));
            usuario.setGama(cursor.getString(1));
            usuario.setMarca(cursor.getString(2));
            usuario.setModelo(cursor.getString(3));
            usuario.setCompa(cursor.getString(4));


            listaUsuario.add(usuario);
        }
        obtenerLista();
    }

    private void obtenerLista() {

        listaInformacion = new ArrayList<String>();

        for (int i=0; i<listaUsuario.size();i++){
            listaInformacion.add(listaUsuario.get(i).getImei()+" - "+listaUsuario.get(i).getGama());

        }
    }
}
