package com.example.ejemplosqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ejemplosqlite.utilidades.utilidades;

public class Consulta_individual extends AppCompatActivity {
    TextView IMEI,GAMA,MARCA,MODELO,COMPA;
    Button botonBuscar,botonEliminar,botonActualizar;
    ConexionSQLiteHelper conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta_individual);

         conn= new ConexionSQLiteHelper(this, "bd_usuarios",null,1);


        IMEI =(TextView)findViewById(R.id.editTextIdBuscar);
        GAMA =(TextView)findViewById(R.id.editTextNombreBuscar);
        MARCA =(TextView)findViewById(R.id.editTextTelefonoBuscar);
        MODELO=(TextView)findViewById(R.id.editText1)
        ;
      COMPA =(TextView)findViewById(R.id.editText2);


        botonBuscar = (Button)findViewById(R.id.btnBuscar);
        botonActualizar = (Button)findViewById(R.id.btnActualizar);
        botonEliminar = (Button)findViewById(R.id.btnEliminar);

        botonActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Actualizar();

            }

            private void Actualizar() {
                SQLiteDatabase db=conn.getWritableDatabase();
                String[] parametros = {IMEI.getText().toString()};
                ContentValues values = new ContentValues();
                values.put(utilidades.CAMPO_Gama,GAMA.getText().toString());
                values.put(utilidades.CAMPO_Marca,MARCA.getText().toString());
                values.put(utilidades.CAMPO_Modelo,MODELO.getText().toString());
                values.put(utilidades.CAMPO_Compa,COMPA.getText().toString());

                db.update(utilidades.TABLA_Tegnologia,values,utilidades.CAMPO_IMEI+"=?",parametros);
                Toast.makeText(getApplicationContext(),"Ya se Actualizaron los datos",Toast.LENGTH_LONG).show();
                Limpiar();
                db.close();

            }
            private void Limpiar() {
                GAMA.setText("");
                MODELO.setText("");
                COMPA.setText("");
                MARCA.setText("");
            }


        });
        botonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Eliminar();

            }

            private void Eliminar() {
                SQLiteDatabase db=conn.getWritableDatabase();
                String[] parametros = {IMEI.getText().toString()};

                db.delete(utilidades.TABLA_Tegnologia,utilidades.CAMPO_IMEI+"=?",parametros);
                Toast.makeText(getApplicationContext(),"Se elimino el Usuario",Toast.LENGTH_LONG).show();
                Limpiar();
                db.close();




            }
            private void Limpiar() {
                GAMA.setText("");
              MARCA.setText("");
              MODELO.setText("");
              COMPA.setText("");
            }
        });

        botonBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            consultar();
            }

            private void consultar() {

                SQLiteDatabase db=conn.getReadableDatabase();
                String[] parametros={IMEI.getText().toString()};
                String[] campos= {utilidades.CAMPO_Gama,utilidades.CAMPO_Marca,utilidades.CAMPO_Modelo,utilidades.CAMPO_Compa, };

                try{
                    Cursor cursor = db.query(utilidades.TABLA_Tegnologia,campos, utilidades.CAMPO_IMEI+"=?",parametros,null,null,null);
                    cursor.moveToFirst();

                 GAMA.setText(cursor.getString(0));
               MARCA.setText(cursor.getString(1));
                    MODELO.setText(cursor.getString(2));
                    COMPA.setText(cursor.getString(3));
                    cursor.close();

                }catch (Exception e){

                    Toast.makeText(getApplicationContext(),"El documento no existe",Toast.LENGTH_LONG).show();
                    Limpiar();



                }



            }

            private void Limpiar() {
               GAMA.setText("");
              MARCA.setText("");
              MODELO.setText("");
              COMPA.setText("");
            }
        });

    }
}
