package com.example.ejemplosqlite.entidades;

public class Usuario {
    private Integer imei;
    private String gama;
    private String marca;
    private String modelo;
    private String compa;

    public Usuario() {
        this.imei = imei;
        this.gama = gama;
        this.marca = marca;
        this.modelo = modelo;
        this.compa = compa;
    }

    public Integer getImei() {
        return imei;
    }

    public void setImei(Integer imei) {
        this.imei = imei;
    }

    public String getGama() {
        return gama;
    }

    public void setGama(String gama) {
        this.gama = gama;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getCompa() {
        return compa;
    }

    public void setCompa(String compa) {
        this.compa = compa;
    }
}
